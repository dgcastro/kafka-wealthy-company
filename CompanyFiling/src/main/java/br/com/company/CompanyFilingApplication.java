package br.com.company;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CompanyFilingApplication {

	public static void main(String[] args) {
		SpringApplication.run(CompanyFilingApplication.class, args);
	}

}
