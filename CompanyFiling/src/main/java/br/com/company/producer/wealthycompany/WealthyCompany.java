package br.com.company.producer.wealthycompany;


public class WealthyCompany {
    private String name;
    private String uf;
    private String email;
    private double companyValue;
    private String cnpj;

    public WealthyCompany() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getCompanyValue() {
        return companyValue;
    }

    public void setCompanyValue(double companyValue) {
        this.companyValue = companyValue;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }
}
