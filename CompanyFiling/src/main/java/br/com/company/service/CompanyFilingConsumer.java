package br.com.company.service;

import br.com.company.producer.wealthycompany.WealthyCompany;
import org.apache.kafka.common.protocol.types.Field;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import au.com.bytecode.opencsv.CSVWriter;


import java.io.FileWriter;
import java.io.IOException;

@Component
public class CompanyFilingConsumer {
    @KafkaListener(topics = "spec3-danilo-gouveia-3", groupId = "CompanyValidation")
    public void consumer (@Payload WealthyCompany wealthyCompany){
        String CSV = "/home/company.csv";
        try{
            CSVWriter writer = new CSVWriter(new FileWriter(CSV, true));
            String record = wealthyCompany.getName()
                    +","
                    +wealthyCompany.getCnpj()
                    +","
                    +wealthyCompany.getEmail()
                    +","
                    +wealthyCompany.getUf()
                    +","
                    +String.valueOf(wealthyCompany.getCompanyValue());
            writer.writeNext(record);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
