package br.com.company.controller;

import br.com.company.producer.company.Company;
import br.com.company.service.CompanyRegistryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CompanyRegistryController {
    @Autowired
    CompanyRegistryService companyRegistryService;

    @PostMapping("/company/registry")
    public boolean companyRegistry(@RequestBody Company company){
        companyRegistryService.sendToKafka(company);
        return true;
    }
}
