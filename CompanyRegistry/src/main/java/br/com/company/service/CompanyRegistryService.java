package br.com.company.service;


import br.com.company.producer.company.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class CompanyRegistryService {
    @Autowired
    private KafkaTemplate<String, Company> producer;

    public void sendToKafka(Company company){
        producer.send("spec3-danilo-gouveia-2", company);
    }
}
