package br.com.company;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class CompanyValidationApplication {

	public static void main(String[] args) {
		SpringApplication.run(CompanyValidationApplication.class, args);
	}

}
