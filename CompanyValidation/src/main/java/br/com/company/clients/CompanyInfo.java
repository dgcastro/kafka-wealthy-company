package br.com.company.clients;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CompanyInfo {

    @JsonProperty("nome")
    private String name;

    @JsonProperty("uf")
    private String uf;

    @JsonProperty("email")
    private String email;

    @JsonProperty("capital_social")
    private double companyValue;

    @JsonProperty("cnpj")
    private String cnpj;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getCompanyValue() {
        return companyValue;
    }

    public void setCompanyValue(double companyValue) {
        this.companyValue = companyValue;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }
}
