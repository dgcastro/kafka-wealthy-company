package br.com.company.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "companyInfo", url = "https://www.receitaws.com.br/")
public interface CompanyInfoClient {

    @GetMapping("/v1/cnpj/{cnpj}")
    CompanyInfo getByCnpj(@PathVariable String cnpj);
}
