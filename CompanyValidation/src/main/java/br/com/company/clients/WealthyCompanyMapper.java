package br.com.company.clients;

import br.com.company.producer.wealthycompany.WealthyCompany;
import org.springframework.stereotype.Component;

@Component
public class WealthyCompanyMapper {
    public WealthyCompany toWealthyCompany(CompanyInfo companyInfo){
        WealthyCompany wealthyCompany = new WealthyCompany();
        wealthyCompany.setName(companyInfo.getName());
        wealthyCompany.setEmail(companyInfo.getEmail());
        wealthyCompany.setUf(companyInfo.getUf());
        wealthyCompany.setCompanyValue(companyInfo.getCompanyValue());
        wealthyCompany.setCnpj(companyInfo.getCnpj());
        return wealthyCompany;
    }
}
