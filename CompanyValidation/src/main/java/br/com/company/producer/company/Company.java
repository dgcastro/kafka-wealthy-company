package br.com.company.producer.company;

public class Company {
    private String cnpj;

    public Company() {
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }
}
