package br.com.company.service;

import br.com.company.clients.CompanyInfo;
import br.com.company.clients.CompanyInfoClient;
import br.com.company.clients.WealthyCompanyMapper;
import br.com.company.producer.company.Company;
import br.com.company.producer.wealthycompany.WealthyCompany;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class CompanyValidationConsumer {
    @Autowired
    private CompanyValidationProducer companyValidationProducer;

    @Autowired
    private CompanyInfoClient companyInfoClient;

    @Autowired
    private WealthyCompanyMapper wealthyCompanyMapper;

    @KafkaListener(topics = "spec3-danilo-gouveia-2", groupId = "CompanyValidation")
    public void cosumer(@Payload Company company){
        try{
            CompanyInfo companyInfo = companyInfoClient.getByCnpj(company.getCnpj());
            WealthyCompany wealthyCompany = wealthyCompanyMapper.toWealthyCompany(companyInfo);
            if(wealthyCompany.getCompanyValue() > 999999.99) {
                System.out.println(wealthyCompany.toString());
                companyValidationProducer.sendToKafka(wealthyCompany);
            }else {
                System.out.println(wealthyCompany.getName() +"\n"+wealthyCompany.getCnpj()+"\n"+wealthyCompany.getCompanyValue());
            }
        }catch (FeignException.BadRequest badRequest){
            System.out.println(badRequest.getMessage());
        }
    }
}
