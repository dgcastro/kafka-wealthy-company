package br.com.company.service;


import br.com.company.producer.wealthycompany.WealthyCompany;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class CompanyValidationProducer {

    @Autowired
    private KafkaTemplate<String, WealthyCompany> producer;

    public void sendToKafka(WealthyCompany wealthyCompany){
        producer.send("spec3-danilo-gouveia-3", wealthyCompany);
    }
}
